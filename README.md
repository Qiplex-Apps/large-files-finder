<div align="center">

<h1> <a href="https://qiplex.com/software/large-files-finder/">Large Files Finder</a> </h1>
  
<h3> Find large files easily on Windows, macOS and Linux! </h3>

You can get the app on 
<a href="https://gitlab.com/Qiplex-Apps/large-files-finder/-/releases">GitLab</a>
or on 
<a href="https://qiplex.com/software/large-files-finder/">my website</a>
<br>Code comes soon.

![Large Files Finder](https://qiplex.com/assets/img/app/main/large-files-finder-app.png)

<h4>Check out the app features below: </h4>

![Large Files Finder - Features](https://user-images.githubusercontent.com/32670415/147223068-a7116f04-1706-4cb6-b06c-d61b6d4cbc90.png)
  
</div>
